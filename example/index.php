<?php


require_once  '../vendor/autoload.php' ;

$a = new \bioshock\xlsxWrapper\xlsxWrapper() ;

$a->setColumnOrder(['one','two','three']) ;
$a->setSheetTitle('this is title') ;
$a->addRow(['one'=>'1','three'=>'3', 'two'=>'2' , 'gf'=>'fv']) ;
$a->addRow(['one'=>'14','three'=>'32', 'two'=>'2' , 'gf'=>'11']) ;

$a->save('this.xlsx');