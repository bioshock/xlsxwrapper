<?php

namespace bioshock\xlsxWrapper;

/**
 * Class xlsxWrapper
 * @package bioshock\xlsxWrapper
 */
class xlsxWrapper
{

    protected $headerRow;
    protected $headerWritten = false;
    protected $currentSheet;
    protected $index;
    protected $objPHPExcel;


    function __construct()
    {

        $this->objPHPExcel = new \PHPExcel();
        $this->currentSheet = $this->objPHPExcel->getActiveSheet();
        $this->index = 1;
        $this->headerRow = null ;

    }

    /**
     * @param $title string
     */
    function setSheetTitle($title)
    {
        $this->currentSheet->setTitle($title);
    }


    /**
     * @param $headerRow array
     */
    function setColumnOrder($headerRow)
    {

        $this->headerRow = $headerRow;

    }

    /**
     * @param $assocArray array
     * @param null $defaultValue string}null
     * @return array
     */
    protected function reOrderRow($assocArray, $defaultValue = null)
    {

        $newAssocArray = array();

        foreach ($this->headerRow as $headerCell) {

            if (isset($assocArray[$headerCell])) {
                $newAssocArray[$headerCell] = $assocArray[$headerCell];
            } else {
                $newAssocArray[$headerCell] = $defaultValue;
            }
        }
        return $newAssocArray;
    }


    /**
     * @param $rowArray array
     * @throws \PHPExcel_Exception
     */
    function addRow($rowArray)
    {

        if (!$this->headerWritten && $this->headerRow !== null ) {

            $this->currentSheet->fromArray($this->headerRow);
            $this->index++;
            $this->headerWritten = true;
        }

        if($this->headerRow !== null)
            $rowArray = $this->reOrderRow($rowArray);

        $this->currentSheet->fromArray($rowArray, null, 'A' . $this->index);
        $this->index++;
    }

    /**
     * @param $rowArrays
     * @throws \Exception
     * @internal param array $rowArray
     */
    function addMultipleRows($rowArrays)
    {
        foreach ($rowArrays as $k=>$rowArray){
            if(is_array($rowArray)) {
                $this->addRow($rowArray);
            }else
            {
                throw  new \Exception("Multidimensional array expected : key ".$k .' is not an array');
            }
        }
    }

    /**
     * @param $file string
     * @throws \PHPExcel_Reader_Exception
     */
    function save($file)
    {


        $objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter->save($file);

    }

}